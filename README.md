Here is a list of my personal projects:

- Distributed Web Application – Integrated Energy Monitoring Platform for Households
    https://gitlab.com/ds2021_30643_state_marin/ds2021_30643_state_marin_3_backend
    https://gitlab.com/ds2021_30643_state_marin/ds2021_30643_state_marin_2_frontend
- Customer Management Application (minimizing queue waiting time) 
    https://gitlab.com/gg7urb0/pt2020_30225_marin_state_assignment_2
- Order Management Application(customer order processing)
    https://gitlab.com/gg7urb0/pt2020_30225_marin_state_assignment_3
- Restaurant Management Application 
    https://gitlab.com/gg7urb0/pt2020_30225_marin_state_assignment_4
- Analysis of a person's behavior recorded by a set of sensors 
    https://gitlab.com/gg7urb0/pt2020_30225_marin_state_assignment_5